import React, { useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getPosts, getPostById } from "../../redux/actions/posts/postActions";
import { Link } from "react-router-dom";

const ListPost = (props) => {
    useEffect(() => {
        props.getPosts();
    });
    return (
        <div className="ListPostWrapper">
            {props.data.map((post) => {
                return (
                    <div key={post.id}>
                        Tutorial : {post.id}
                        {" : "}
                        <Link to={`/posts/${post.id}`}>{post.title}</Link>
                        <br />
                    </div>
                );
            })}
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        data: state.data,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            getPosts,
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(ListPost);
